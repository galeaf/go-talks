package subs

import "errors"

func Find(substring string, target string) ([]int, error) {

	var position []int

	if len(substring) > len(target) {
		return position, errors.New("Неа!")
	}

	if len(substring) == len(target) && substring != target {
		return position, errors.New("Опять Неа!")
	}

	for i := 0; i < len(target); i++ {
		match := true

		for j := 0; j < len(substring); j++ {
			if substring[j] != target[i+j] {
				match = false
				break
			}
		}

		if match {
			position = append(position, i)
		}
	}

	return position, nil
}
