package main

import (
	"testing"

	"./subs"
)

func TestFindSubstring(t *testing.T) {
	target := "asdasag"
	substring := "a"
	find := []int{0, 3, 5}

	result, error := subs.Find(substring, target)

	if error != nil {
		t.Error(error)
	}

	for index, value := range result {
		if find[index] != value {
			t.Error("Да по-любому нет!")
		}
	}
}

func TestEmptyResultArray(t *testing.T) {
	target := "other"
	substring := "zzz"

	result, error := subs.Find(substring, target)

	if error != nil {
		t.Error(error)
	}

	if len(result) != 0 {
		t.Error("Да по-любому нет!")
	}
}

func TestErrorIfSubstringIsLongerTarget(t *testing.T) {
	target := "zzz"
	substring := "other"

	_, error := subs.Find(substring, target)

	if error == nil {
		t.Error("Да по-любому нет!")
	}
}

func TestErrorIfSubstringAndTargetSizeEqualButStringsIsNotEquals(t *testing.T) {
	target := "zzz"
	substring := "yyy"

	_, error := subs.Find(substring, target)

	if error == nil {
		t.Error("Да по-любому нет!")
	}
}
